# Meinungsmacher

## Rest Endpoints
* `GET` - `http://localhost:8080/topics` - Show topics of the day
* `GET` - `http://localhost:8080/votes` - Retrieve all UserVotings that have been made so far.
* `POST` - `http://localhost:8080/votes` - Submit a user's voting.
    Request Json should look like:
    <code>
        {
            "userId": "testUserId",
            "articleId": "articleId",
            "topicId": "testTopicId",
            "answer": 1 // the list's item's number of the selected answer
        }
    </code>
* `GET` - `http://localhost:8080/votes/results/topic/{topicId}/user/{userId}` - VotingResults for articles user has read

## View Endpoints

* `http://localhost:8080/articlesView/topic/t1` - View with (Many) Articles by topic
* `http://localhost:8080/articlesView/article/<articleId>` - View with one article and topic; Model contains `article` and `topic`
* `GET` - `http://localhost:8080/votesView/results/topic/{topicId}/user/{userId}` - VotingResults for articles user has read


## Member von Domain-Objekten
* User
    * id;
    * name;  
    * email;
    * pointsPerTopic[{ topicId.id : &lt;countPoints&gt; }];
    * challengesAccomplished[ &lt;topicId&gt; ];
    * articlesRead [articleId] - The articles a user already read

* VotingResult
    * topicId.id - topic id;
    * votesPerOption { optionOrdinal (int) : countVotes (int) } - Options ordinal number => count votes
    * answersPerOption { optionOrdinal (int) : textual answer (String ) - Options ordinal number => textual answer