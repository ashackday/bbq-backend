package bbqbackend;

import bbqbackend.appservice.ArticleService;
import bbqbackend.model.repository.ArticleStore;
import bbqbackend.model.repository.TopicStore;
import bbqbackend.model.topic.Challenge;
import bbqbackend.model.topic.Topic;
import bbqbackend.model.topic.TopicId;
import bbqbackend.service.articlecrawling.ArticleCrawlerDelegator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;

/**
 *
 */
@Component
@Slf4j
public class StaticStoreDatainitializer {

    private static final int ARTICLE_FETCH_COUNT = 100;

    @Autowired
    ArticleService articlesService;
    @Autowired
    TopicStore topicStore;
    @Autowired
    ArticleStore articleStore;

    @Autowired
    ArticleCrawlerDelegator articleCrawlerDelegator;

    @PostConstruct
    public void initStores(){
        initTopics();
        initArticles();
    }

    private void initTopics() {
        TopicId tId_1 = new TopicId("t1");
        TopicId tId_2 = new TopicId("t2");
        TopicId tId_3 = new TopicId("t3");
        TopicId tId_4 = new TopicId("t4");
        TopicId tId_5 = new TopicId("t5");
        TopicId tId_6 = new TopicId("t6");
        TopicId tId_7 = new TopicId("t7");

        topicStore.addTopics(
                Topic.builder()
                        .topicId(tId_7)
                        .name("Ölpreis")
                        .challenge(Challenge.builder()
                                .question("Wären Sie bereit mehr Geld für Kraftstoff auszugeben um die Krise zu lindern?")
                                .options(Arrays.asList("xx", "yy"))
                                .build()
                        )
                        .build()
        );

        topicStore.addTopics(
                Topic.builder()
                        .topicId(tId_1)
                        .name("Griechenlandpleite")
                        .challenge(Challenge.builder()
                                        .question("Wie stehst Du zum GREXIT?")
                                        .options(Arrays.asList("Ja, endlich raus mit ihnen.", "Scheitert Griechenland, scheitert Europa"))
                                        .build()
                        )
                        .build()
        );

        topicStore.addTopics(
                Topic.builder()
                        .topicId(tId_2)
                        .name("Amerikawahl")
                        .challenge(Challenge.builder()
                                        .question("Würdest Du nach Lesen des Artikels Hillary Clinton wählen?")
                                        .options(Arrays.asList("Ja, total, die hat bestimmt auch Zigarren im Angebot", "Nein, lieber nicht, die ist so demokratisch."))
                                        .build()
                        )
                        .build()
        );
        topicStore.addTopics(
                Topic.builder()
                        .topicId(tId_3)
                        .name("NSA-Affäre")
                        .challenge(Challenge.builder()
                                        .question("Welche Institution würden Sie aufgrund des Fehlverhaltens in der NSA Affäre einstampfen??")
                                        .options(Arrays.asList("BND", "Keine"))
                                        .build()
                        )
                        .build()
        );
        topicStore.addTopics(
                Topic.builder()
                        .topicId(tId_4)
                        .name("Apple Music")
                        .challenge(Challenge.builder()
                                        .question("Ist Apple Music besser als Spotify??")
                                        .options(Arrays.asList("xx", "yy"))
                                        .build()
                        )
                        .build()
        );
        topicStore.addTopics(
                Topic.builder()
                        .topicId(tId_5)
                        .name("TV-Rechte Olympia")
                        .challenge(Challenge.builder()
                                        .question("?")
                                        .options(Arrays.asList("xx", "yy"))
                                        .build()
                        )
                        .build()
        );
        topicStore.addTopics(
                Topic.builder()
                        .topicId(tId_6)
                        .name("Homo-Ehe in den USA")
                        .challenge(Challenge.builder()
                                        .question("?")
                                        .options(Arrays.asList("xx", "yy"))
                                        .build()
                        )
                        .build()
        );
    }

    private void initArticles() {
        for(Topic t :topicStore.todaysTopics()) {
            articlesService.addArticle(t.getTopicId(), articleCrawlerDelegator.articlesByTopic(t, ARTICLE_FETCH_COUNT));
            log.info("ArticleStore now contains {} articles!!", articleStore.countArticles());
        }
    }


}
