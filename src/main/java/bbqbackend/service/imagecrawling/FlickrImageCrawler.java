package bbqbackend.service.imagecrawling;

import bbqbackend.model.topic.Topic;
import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.photos.Photo;
import com.flickr4java.flickr.photos.PhotoList;
import com.flickr4java.flickr.photos.SearchParameters;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 */
@Slf4j
@Component
public class FlickrImageCrawler {

    final String API_KEY = "2ff949444439366df54d67f9ad066c66";
    final String SHARED_SECRET = "bb57ea5ff58d47e6";

    public Set<String> imagesForTopic(Topic topic, int count, int offset) {

        Flickr flickr = new Flickr(API_KEY, SHARED_SECRET, new REST());

        SearchParameters params = new SearchParameters();
        params.setText(topic.getName());

        try {
            final PhotoList<Photo> search = flickr.getPhotosInterface().search(params, count, offset);
            return search.stream().map(p -> p.getMediumUrl()).collect(Collectors.toSet());
        } catch (FlickrException e) {
            log.error("Error requesting Flickr API", e);
            return new HashSet<>();
        }
//        TestInterface testInterface = flickr.getTestInterface();
//        Collection results = testInterface.echo(Collections.EMPTY_MAP);
    }
}
