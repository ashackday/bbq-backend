package bbqbackend.service.articlecrawling.qwant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import bbqbackend.model.article.Article;
import bbqbackend.model.article.ArticleId;
import bbqbackend.service.articlecrawling.qwant.response.QwantItem;
import bbqbackend.service.articlecrawling.qwant.response.QwantResponse;

@Component
@Slf4j
public class QwantService {

	public List<Article> callNews(int count, String source, String locale, int offset, String search) {
		RestTemplate restTemplate = new RestTemplate();
		List<Article> articles = new ArrayList<>();
		QwantResponse forObject = restTemplate.getForObject(
				"https://api.qwant.com/api/search/news?count=" + count
						+ "&f=" + source
						+ "&locale=" + locale
						+ "&offset=" + offset
						+ "&q=" + search,
				QwantResponse.class);
		List<QwantItem> items = forObject.getData().getResult().getItems();
		log.info("Fetched {} QuantItems by topic {}.", items.size(), search);
		items.parallelStream()
				.forEach(item -> {
			try {
				Document doc = Jsoup.connect(item.getUrl()).get();

				Elements select = doc.select(".article");

						Article build = Article.builder()
								.headline(item.getTitle())
								.articleId(new ArticleId(item.get_id()))
								.content(select.text())
								.teaserText(item.getDesc())
								.build();

						if (!item.getMedia().isEmpty()) {
							build.setImageUrl(item.getMedia().get(0).getUrl());
						}
						if (!build.getContent().equalsIgnoreCase("")) {
							articles.add(build);
						}
			}
			catch (IOException e) {
			}
		});

		return articles;
	}

}
