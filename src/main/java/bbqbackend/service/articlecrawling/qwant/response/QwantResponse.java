package bbqbackend.service.articlecrawling.qwant.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;




@Data
@JsonIgnoreProperties(
		ignoreUnknown = true)
public class QwantResponse {

	private String status;

	private QwantData data;


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public QwantData getData() {
		return data;
	}


	public void setData(QwantData data) {
		this.data = data;
	}
}
