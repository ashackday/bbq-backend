package bbqbackend.service.articlecrawling.qwant.response;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;



@Data
@JsonIgnoreProperties(
		ignoreUnknown = true)
public class QwantItem {

	private String title;
	private String url;
	private String desc;
	private Date date;
	private String domain;
	private String _id;
	
	private List<QwantMedia> media;
	
}
