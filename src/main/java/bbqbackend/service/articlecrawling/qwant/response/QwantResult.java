package bbqbackend.service.articlecrawling.qwant.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;



@JsonIgnoreProperties(
		ignoreUnknown = true)
@Data
public class QwantResult {

	private List<QwantItem> items;


	public List<QwantItem> getItems() {
		return items;
	}


	public void setItems(List<QwantItem> items) {
		this.items = items;
	}
}
