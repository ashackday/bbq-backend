package bbqbackend.service.articlecrawling.qwant.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;



@Data
@JsonIgnoreProperties(
		ignoreUnknown = true)
public class QwantData {

	private QwantResult result;


	public QwantResult getResult() {
		return result;
	}


	public void setResult(QwantResult result) {
		this.result = result;
	}

}
