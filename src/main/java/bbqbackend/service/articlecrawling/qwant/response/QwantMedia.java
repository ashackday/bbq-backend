package bbqbackend.service.articlecrawling.qwant.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(
		ignoreUnknown = true)
public class QwantMedia {

	 private String url;
}
