package bbqbackend.service.articlecrawling;

import bbqbackend.model.article.Article;
import bbqbackend.model.topic.Topic;
import bbqbackend.service.articlecrawling.ipool.IPoolService;
import bbqbackend.service.articlecrawling.qwant.QwantService;
import bbqbackend.service.imagecrawling.FlickrImageCrawler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Component
@Slf4j
public class ArticleCrawlerDelegator {

    @Autowired
    private QwantService qwantService;
	@Autowired
	private IPoolService poolService;
	@Autowired
	private FlickrImageCrawler imageCrawler;

    public List<Article> articlesByTopic(Topic topic, int count){
        List<Article> articlesByQwant = articlesByQwant(topic, count);
		log.info("Returned {} QuantItems.", articlesByQwant.size());
		List<Article> articlesByIPool = articlesByIPool(topic);
		log.info("Returned {} IPool Items.", articlesByIPool.size());

		List<Article> result = new ArrayList<>();
		result.addAll(articlesByQwant);
		result.addAll(articlesByIPool);
		addImages2Articles(result, topic);
		return result;
    }

	private void addImages2Articles(List<Article> articles, Topic topic) {

		final List<String> imgUrls = new ArrayList<>(imageCrawler.imagesForTopic(topic, 10, 0));
		log.info("Retrieved {} images from Flickr for Topic {}.", imgUrls.size(), topic.getName());
		if(imgUrls.isEmpty())return;
		for (int i = 0; i < articles.size(); i++) {
			if(articles.get(i) == null)continue;
			articles.get(i).setImageUrl(imgUrls.get(i%imgUrls.size()));
		}
	}

	private List<Article> articlesByQwant(Topic topic, int count) {
        return qwantService.callNews(count, "welt.de", "de_DE", 0, topic.getName());
    }
	private List<Article> articlesByIPool(Topic topic) {
		return poolService.callIPool(topic.getName());
	}



}
