package bbqbackend.service.articlecrawling.ipool.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(
		ignoreUnknown = true)
public class IPoolDocument {

	private String content;
	private String title;
	private String leadtext;
	private String identifier;
	
}
