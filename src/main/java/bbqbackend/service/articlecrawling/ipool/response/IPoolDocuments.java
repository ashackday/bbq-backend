package bbqbackend.service.articlecrawling.ipool.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(
		ignoreUnknown = true)
public class IPoolDocuments {

	private List<IPoolDocument> documents;
}
