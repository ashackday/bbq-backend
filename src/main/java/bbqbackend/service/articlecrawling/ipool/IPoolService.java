package bbqbackend.service.articlecrawling.ipool;


import bbqbackend.model.article.Article;
import bbqbackend.model.article.ArticleId;
import bbqbackend.service.articlecrawling.ipool.response.IPoolDocument;
import bbqbackend.service.articlecrawling.ipool.response.IPoolDocuments;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
//import oauth.signpost.OAuthConsumer;
//import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
//

@Component
public class IPoolService {

    public static final String HTTPS_IPOOL_BASE_URL = "https://frontend-api.ipool.asideas.de/";
    public static final String IPOOL_USER = "hackathon";
    public static final String IPOOL_PW = "hackme";

    public List<Article> callIPool(String search) {
        List<Article> articleList = new ArrayList<>();
        IPoolDocuments fromJson = null;
        try {
            SSLContext context = SSLContext.getInstance("SSL");
            context.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());


            // create a consumer object and configure it with the access
            // token and token secret obtained from the service provider
//            OAuthConsumer consumer = new CommonsHttpOAuthConsumer("mediahackday", "1hackpool!");
//            consumer.setTokenWithSecret("", "");
            // create an HTTP request to a protected resource
            HttpGet request = new HttpGet(HTTPS_IPOOL_BASE_URL + "/api/v3/search?q=" + URLEncoder.encode(search, "UTF-8"));
            // sign the request
//            consumer.sign(request);
            // send the request
            DefaultHttpClient httpClient = new DefaultHttpClient();

            // BASIC AUTH
//            Credentials defaultcreds = new UsernamePasswordCredentials("username", "password");
            UsernamePasswordCredentials creds = new UsernamePasswordCredentials(IPOOL_USER, IPOOL_PW);
            request.addHeader(new BasicScheme().authenticate(creds, request));


            HttpResponse response = httpClient.execute(request);
            String responseBody = EntityUtils.toString(response.getEntity());
            Gson gson = new GsonBuilder().create();

            fromJson = gson.fromJson(responseBody, IPoolDocuments.class);

            List<IPoolDocument> documents = fromJson.getDocuments();
            System.out.println(documents.size());
            documents.parallelStream()
                    .filter(element -> {
                        if ("".equalsIgnoreCase(element.getContent())) {
                            return false;
                        } else {
                            return true;
                        }
                    })
                    .forEach(element -> {
                        String teaserText;
                        if (element.getLeadtext() == null || "".equalsIgnoreCase(element.getLeadtext())) {
                            teaserText = element.getContent().substring(0, 200) + "...";
                        } else {
                            teaserText = element.getLeadtext();
                        }

                        Article build = Article.builder()
                                .headline(element.getTitle())
                                .articleId(new ArticleId(element.getIdentifier()))
                                .content(element.getContent())
                                .teaserText(teaserText)
                                .build();
                        articleList.add(build);
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }


        return articleList;
    }

    private static TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }


                @Override
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                    // TODO Auto-generated method stub
                }


                @Override
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                    // TODO Auto-generated method stub
                }
            }
    };

}
