package bbqbackend.appservice;

import bbqbackend.model.article.Article;
import bbqbackend.model.repository.ArticleStore;
import bbqbackend.model.repository.TopicStore;
import bbqbackend.model.topic.Topic;
import bbqbackend.model.topic.TopicId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Collection;

/**
 *
 */
@Component
public class ArticleService {

    @Autowired
    ArticleStore articleStore;
    @Autowired
    TopicStore topicStore;

    public void addArticle(TopicId topicId, Collection<Article> articles) {
        Assert.notNull(topicId);
        Assert.notNull(topicStore.topicById(topicId), "Topic for new article must exist in repo");

        for(Article a : articles){
            if(a == null)continue;
            a.setArticleId(articleStore.newId());
            a.setAssignedTopic(topicId);
            articleStore.addArticles(a);
        }
    }

}
