package bbqbackend.appservice;

import bbqbackend.model.article.Article;
import bbqbackend.model.article.ArticleId;
import bbqbackend.model.repository.ArticleStore;
import bbqbackend.model.repository.TopicStore;
import bbqbackend.model.repository.UserStore;
import bbqbackend.model.repository.VotingStore;
import bbqbackend.model.topic.Topic;
import bbqbackend.model.topic.TopicId;
import bbqbackend.model.user.User;
import bbqbackend.model.user.UserVoting;
import bbqbackend.model.user.VotingResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class VotingService {

    @Autowired
    VotingStore votingStore;
    @Autowired
    TopicStore topicStore;
    @Autowired
    ArticleStore articleStore;

    @Autowired
    UserStore userStore;

    public void addVoting(UserVoting userVoting){
        final User user = userStore.getUser(userVoting.getUserId());
        final Topic topic = topicStore.topicById(userVoting.getTopicId());
        Assert.notNull(user);
        Assert.notNull(topic);
        user.addArticleRead(userVoting.getArticleId());
        user.addPoints(topic, 1);
        votingStore.addVoting(userVoting);
    }

    public Set<UserVoting> allVotes(){
        return votingStore.allVotes();
    }

    public VotingResult votingResultForUsersArticles(String userId, TopicId topicId) {
        final Topic topic = topicStore.topicById(topicId);
        final User user = userStore.getUser(userId);
        Assert.notNull(topic);
        Assert.notNull(user);
        final Set<ArticleId> articlesReadForTopic = articleStore.articlesByTopic(topicId).stream()
                .filter( a -> user.getArticlesRead().contains(a.getArticleId()))
                .map( a -> a.getArticleId())
                .collect(Collectors.toSet());


        final Set<UserVoting> userVotings = votingStore.votesForArticles(articlesReadForTopic);
        return VotingResult.fromUserVotings(topic, articlesReadForTopic, userVotings, articleStore);
    }
}