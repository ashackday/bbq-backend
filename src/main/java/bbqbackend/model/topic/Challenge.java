package bbqbackend.model.topic;

import lombok.Data;
import lombok.experimental.Builder;

import java.util.List;

/**
 * VALUE OBJECT!
 */
@Data
@Builder
public class Challenge {

    private final int countPointsToWinChallenge = 2;

    private String question;
    private List<String> options;

}
