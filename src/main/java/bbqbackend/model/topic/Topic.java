package bbqbackend.model.topic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Builder;

/**
 * ENTITY!
 */
@Builder
@AllArgsConstructor
@Getter
@ToString
public class Topic {

    private TopicId topicId;
    private String name;
    private Challenge challenge;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Topic)) {
            return false;
        }

        Topic topic = (Topic) o;

        return !(topicId != null ? !topicId.equals(topic.topicId) : topic.topicId != null);

    }
    @Override
    public int hashCode() {
        return topicId != null ? topicId.hashCode() : 0;
    }
}
