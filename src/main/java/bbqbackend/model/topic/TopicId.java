package bbqbackend.model.topic;

/**
 *
 */
public class TopicId {

    String tId;

    public TopicId(String tId) {
        this.tId = tId;
    }

    public String gettId() {
        return tId;
    }
    public void settId(String tId) {
        this.tId = tId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TopicId)) {
            return false;
        }

        TopicId topicId = (TopicId) o;

        return !(tId != null ? !tId.equals(topicId.tId) : topicId.tId != null);

    }
    @Override
    public int hashCode() {
        return tId != null ? tId.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "TopicId{" +
                "tId='" + tId + '\'' +
                '}';
    }
}
