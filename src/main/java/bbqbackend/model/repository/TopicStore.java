package bbqbackend.model.repository;

import bbqbackend.model.topic.Topic;
import bbqbackend.model.topic.TopicId;

import java.util.Set;

/**
 *
 */
public interface TopicStore {

    void addTopics(Topic... topics);
    Set<Topic> todaysTopics();
    Topic topicById(TopicId tId);
}
