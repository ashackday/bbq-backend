package bbqbackend.model.repository;

import bbqbackend.model.article.ArticleId;
import bbqbackend.model.user.UserVoting;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 */
@Component
public class VotingStoreInMem implements VotingStore {

    private List<UserVoting> store = new ArrayList<>();

    @Override
    public void addVoting(UserVoting userVoting) {
        Assert.hasText(userVoting.getUserId());
        Assert.notNull(userVoting.getTopicId());
        Assert.notNull(userVoting.getArticleId());
        Assert.notNull(userVoting.getAnswer());
        store.add(userVoting);
    }

    @Override
    public Set<UserVoting> allVotes() {
        return Collections.unmodifiableSet(new HashSet<>(store));
    }

    @Override
    public Set<UserVoting> votesForArticle(final ArticleId articleId) {
        return store.stream().filter( v -> v.getArticleId().equals(articleId)).collect(Collectors.toSet());
    }
    @Override
    public Set<UserVoting> votesForArticles(final Set<ArticleId> articleIds) {
        return store.stream().filter(v -> articleIds.contains(v.getArticleId())).collect(Collectors.toSet());
    }
}
