package bbqbackend.model.repository;

import bbqbackend.model.topic.Challenge;
import bbqbackend.model.topic.Topic;
import bbqbackend.model.topic.TopicId;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 */
@Component
public class TopicStoreInMem implements TopicStore{

    private Map<TopicId, Topic> storedTopics = new HashMap<>();

    @Override
    public void addTopics(Topic... topics) {
        Assert.notNull(topics);
        Arrays.asList(topics).stream().forEach(t -> {
            Assert.notNull(t.getTopicId());
            Assert.notNull(t.getChallenge());
            storedTopics.put(t.getTopicId(), t);
        });
    }

    @Override
    public Set<Topic> todaysTopics() {
        return new HashSet<>(storedTopics.values());
    }

    @Override
    public Topic topicById(TopicId tId) {
        return storedTopics.get(tId);
    }
}
