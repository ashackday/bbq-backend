package bbqbackend.model.repository;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import bbqbackend.model.user.User;
import groovy.lang.Singleton;

@Singleton
@Component
public class UserStoreInMemory implements UserStore {

	private Map<String, User> userStore = new HashMap<>();


	public User getUser(String id) {
		return userStore.get(id);
	}


	public void addUser(User user) {
		userStore.put(user.getId(), user);
	}

}
