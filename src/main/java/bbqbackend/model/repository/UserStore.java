package bbqbackend.model.repository;

import bbqbackend.model.user.User;



public interface UserStore {

	public User getUser(String key);


	public void addUser(User user);

}
