package bbqbackend.model.repository;

import bbqbackend.model.article.Article;
import bbqbackend.model.article.ArticleId;
import bbqbackend.model.topic.Topic;
import bbqbackend.model.topic.TopicId;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 *
 */
@Controller
public class ArticleStoreInMem implements ArticleStore{

    private Map<ArticleId, Article> store = new HashMap<>();

    public void addArticles(Article... articles) {
        Assert.notNull(articles);
        Arrays.asList(articles).stream().forEach(a -> {
            Assert.notNull(a.getArticleId());
            Assert.notNull(a.getAssignedTopic());
            store.put(a.getArticleId(), a);
        });
    }

    @Override
    public Set<Article> articlesByTopic(final TopicId topic) {
        Set<Article> res = store.values().stream()
                .filter(a -> a.getAssignedTopic().equals(topic))
                .collect(Collectors.toSet());
        return res;
    }
    @Override
    public Article articleById(ArticleId articleId) {
        return store.get(articleId);
    }

    @Override
    public int countArticles() {
        return store.size();
    }

    public ArticleId newId() {
        return new ArticleId("ArticleId-" + UUID.randomUUID());
    }

}
