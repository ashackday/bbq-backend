package bbqbackend.model.repository;

import bbqbackend.model.article.ArticleId;
import bbqbackend.model.user.UserVoting;

import java.util.Set;

/**
 *
 */
public interface VotingStore {

    void addVoting(UserVoting userVoting);
    Set<UserVoting> allVotes();
    Set<UserVoting> votesForArticle(ArticleId articleId);

    Set<UserVoting> votesForArticles(Set<ArticleId> articleIds);
}
