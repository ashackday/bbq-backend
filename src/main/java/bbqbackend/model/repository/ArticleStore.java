package bbqbackend.model.repository;

import bbqbackend.model.article.Article;
import bbqbackend.model.article.ArticleId;
import bbqbackend.model.topic.Topic;
import bbqbackend.model.topic.TopicId;

import java.util.Set;

/**
 *
 */
public interface ArticleStore {

    ArticleId newId();

    void addArticles(Article... articles);

    Set<Article> articlesByTopic(TopicId topic);

    Article articleById(ArticleId articleId);

    int countArticles();
}
