package bbqbackend.model.article;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 */
@Data
@AllArgsConstructor
public class ArticleId {

    private String id;

}
