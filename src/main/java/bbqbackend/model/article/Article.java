package bbqbackend.model.article;

import bbqbackend.model.topic.TopicId;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

/**
 * ENTITY
 */
@Getter
@Setter
@ToString
@Builder
public class Article {

    ArticleId articleId;
    String headline;
    String teaserText;
    String teaserImg;
    String content;
    String imageUrl;
    TopicId assignedTopic;

    public void setArticleId(ArticleId articleId) {
        this.articleId = articleId;
    }

    public void setAssignedTopic(TopicId assignedTopic) {
        this.assignedTopic = assignedTopic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Article)) {
            return false;
        }

        Article article = (Article) o;

        return !(articleId != null ? !articleId.equals(article.articleId) : article.articleId != null);

    }
    @Override
    public int hashCode() {
        return articleId != null ? articleId.hashCode() : 0;
    }

}
