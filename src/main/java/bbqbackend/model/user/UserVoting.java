package bbqbackend.model.user;

import bbqbackend.model.article.ArticleId;
import bbqbackend.model.topic.TopicId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Builder;

/**
 *
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserVoting {

    String userId;
    ArticleId articleId;
    TopicId topicId;
    Integer answer;
}
