package bbqbackend.model.user;


import bbqbackend.model.article.ArticleId;
import bbqbackend.model.topic.Topic;
import bbqbackend.model.topic.TopicId;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class User {

	private String id;
	private String name;
	private String email;

	private Map<TopicId, Integer> pointsPerTopic = new HashMap<>();
	private Set<TopicId> challengesAccomplished = new HashSet<>();
	private Set<ArticleId> articlesRead = new HashSet<>();

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public Map<TopicId, Integer> getPointsPerTopic() {
		return pointsPerTopic;
	}
	public Set<TopicId> getChallengesAccomplished() {
		return challengesAccomplished;
	}
	public Set<ArticleId> getArticlesRead() {
		return articlesRead;
	}

	public void addArticleRead(ArticleId articleId) {
		this.articlesRead.add(articleId);
	}
	public void addPoints(Topic topic, int countPoints) {
		if (isChallengeAccomplished(topic.getTopicId())) {
			return;
		}
		pointsPerTopic.putIfAbsent(topic.getTopicId(), 0);
		pointsPerTopic.put(topic.getTopicId(), pointsPerTopic.get(topic.getTopicId()) + countPoints);
		calculateAccomplishedChallenges(topic);
	}

	private boolean isChallengeAccomplished(TopicId topic) {
		return this.challengesAccomplished.contains(topic);
	}

	private void calculateAccomplishedChallenges(Topic topic) {
		if(pointsPerTopic.get(topic.getTopicId()) >= topic.getChallenge().getCountPointsToWinChallenge()) {
			challengesAccomplished.add(topic.getTopicId());
		}
	}


}
