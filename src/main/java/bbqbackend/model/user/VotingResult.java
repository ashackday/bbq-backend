package bbqbackend.model.user;

import bbqbackend.model.article.Article;
import bbqbackend.model.article.ArticleId;
import bbqbackend.model.repository.ArticleStore;
import bbqbackend.model.topic.Challenge;
import bbqbackend.model.topic.Topic;
import bbqbackend.model.topic.TopicId;
import lombok.Data;
import lombok.experimental.Builder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 */
@Data
@Builder
public class VotingResult {

    private TopicId topicId;
    private String topicName;
    private String challengeName;
    private List<ArticleVotes> articleVotes;


    public static VotingResult fromUserVotings(Topic topic, Set<ArticleId> articlesReadForTopic, Collection<UserVoting> userVotings, ArticleStore articleStore){
        Challenge challenge = topic.getChallenge();

        List<ArticleVotes> articleVotes = new ArrayList<>();
        for(ArticleId articleId : articlesReadForTopic) {
            Article article = articleStore.articleById(articleId);

            Map<String, Integer> optionVotes = new HashMap<>();
            for (int i = 0; i < challenge.getOptions().size(); i++) {
                final int finalI = i;
                int countVotesForOption = (int) userVotings.stream().filter(v -> v.getAnswer().equals(finalI)).count();
                String optionText = challenge.getOptions().get(i);
                optionVotes.put(optionText, countVotesForOption);
            }

            articleVotes.add(ArticleVotes.builder()
                    .articleHeadline(article.getHeadline())
                    .articleId(articleId)
                    .optionResults(optionVotes)
                    .build());
        }

        return VotingResult.builder()
                .topicId(topic.getTopicId())
                .topicName(topic.getName())
                .challengeName(topic.getChallenge().getQuestion())
                .articleVotes(articleVotes)
                .build();
    }

    @Data
    @Builder
    public static class ArticleVotes{
        ArticleId articleId;
        String articleHeadline;
        Map<String, Integer> optionResults;
    }

}
