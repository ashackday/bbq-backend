package bbqbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("bbqbackend")
public class BbqBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(BbqBackendApplication.class, args);
    }
}
