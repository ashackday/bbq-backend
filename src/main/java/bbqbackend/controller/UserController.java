package bbqbackend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import bbqbackend.model.article.Article;
import bbqbackend.model.repository.UserStore;
import bbqbackend.model.user.User;
import bbqbackend.service.articlecrawling.ipool.IPoolService;




@RestController
@RequestMapping(
        value = "/user",
        produces = {MediaType.APPLICATION_JSON_VALUE})
public class UserController {

    @Autowired
    private UserStore userStore;


    @RequestMapping(
            method = RequestMethod.POST)
    public void addUser(@RequestBody User user) {
        System.out.println(user);
        userStore.addUser(user);
    }

	@RequestMapping(
			method = RequestMethod.GET)
	public List<Article> test() {
		IPoolService service = new IPoolService();

		return service.callIPool("Griechenlandkrise");
	}


    @RequestMapping(
            method = RequestMethod.GET,
            value = "{id}")
    public User addUser(@PathVariable String id) {
        return userStore.getUser(id);
    }
}
