package bbqbackend.controller;


import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import bbqbackend.model.repository.ArticleStore;
import bbqbackend.model.repository.TopicStore;
import bbqbackend.model.topic.Topic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import bbqbackend.model.article.Article;
import bbqbackend.model.article.ArticleId;
import bbqbackend.model.topic.TopicId;

/**
 *
 */
@Controller
public class ArticlesController {

    private static final Logger log = LoggerFactory.getLogger(ArticlesController.class);

    @Autowired
    private ArticleStore articleStore;
    @Autowired
    private TopicStore topicStore;

    @RequestMapping(value = "articlesView/topic/{topicId}", method = RequestMethod.GET)
    public ModelAndView articleTeasers(
            @PathVariable("topicId") TopicId tId,
            ModelAndView mav) {

        log.info("Hitting articleTeasers with TopicId " + tId);
        mav.setViewName("articleTeasers");

        final Set<Article> articles = articleStore.articlesByTopic(tId);
        log.info("Found " + articles.size() + " articles by topicId" + tId);

        mav.addObject("articles", articles);

        return mav;
    }

    @RequestMapping(value="articlesView/article/{articleId}", method = RequestMethod.GET)
    public ModelAndView getArticle(@PathVariable("articleId") ArticleId articleId, ModelAndView mav){
        log.info("Hitting articlesView/article/" + articleId);
        mav.setViewName("article");

        final Article article = articleStore.articleById(articleId);
        Topic topic = topicStore.topicById(article.getAssignedTopic());

        mav.addObject("article", article);
        mav.addObject("topic", topic);
        return mav;
    }


}
