package bbqbackend.controller;

import bbqbackend.appservice.VotingService;
import bbqbackend.model.article.ArticleId;
import bbqbackend.model.repository.TopicStore;
import bbqbackend.model.topic.Topic;
import bbqbackend.model.topic.TopicId;
import bbqbackend.model.user.UserVoting;
import bbqbackend.model.user.VotingResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Set;

/**
 *
 */
@Controller
@Slf4j
public class VotingCtrl {

    @Autowired
    VotingService votingService;

    @Autowired
    TopicStore topicStore;

    @RequestMapping(value = "votes", method = RequestMethod.POST)
    @ResponseBody
    public void voteForArticle(
            @RequestBody UserVoting voting) {

        log.info("Hitting voteForArticle with vote: " + voting);
        votingService.addVoting(voting);
    }

    @RequestMapping(value = "votes", method = RequestMethod.GET)
    @ResponseBody
    public Set<UserVoting> allVotes(){
        log.info("Hitting allVotes");
        return votingService.allVotes();
    }

    @RequestMapping(value="votes/results/topic/{topicId}/user/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public VotingResult usersVotingResultsForTopics(
            @PathVariable("topicId")TopicId topicId,
            @PathVariable("userId") String userId
    ) {

        log.info("Hitting usersVotingResultsForTopics with topicId{} and userId{}", topicId, userId);
//        return generateMockVotingForArticleAnswer(topicId);

        //TODO: Activate me!!
        return votingService.votingResultForUsersArticles(userId, topicId);
//        return votingService.votingResultForArticle(topicId);
    }

    @RequestMapping(value="votesView/results/topic/{topicId}/user/{userId}", method = RequestMethod.GET)
    public ModelAndView usersVotingResultsForTopicsView(
            @PathVariable("topicId")TopicId topicId,
            @PathVariable("userId")String userId,
            ModelAndView mav){
        mav.setViewName("challengeResult");

        log.info("Hitting usersVotingResultsForTopics with topicId{} and userId{}", topicId, userId);
        mav.addObject("votingResult", votingService.votingResultForUsersArticles(userId, topicId));
        return mav;
    }

    private VotingResult generateMockVotingForArticleAnswer(ArticleId articleId){
        final Topic topic = topicStore.todaysTopics().iterator().next();
        UserVoting v1 = UserVoting.builder()
                .answer(0)
                .articleId(articleId)
                .topicId(topic.getTopicId())
                .userId("egakHier")
                .build();
        UserVoting v2 = UserVoting.builder()
                .answer(0)
                .articleId(articleId)
                .topicId(topic.getTopicId())
                .userId("egakHier")
                .build();
        UserVoting v3 = UserVoting.builder()
                .answer(1)
                .articleId(articleId)
                .topicId(topic.getTopicId())
                .userId("egakHier")
                .build();
        UserVoting v4 = UserVoting.builder()
                .answer(1)
                .articleId(articleId)
                .topicId(topic.getTopicId())
                .userId("egakHier")
                .build();
        UserVoting v5 = UserVoting.builder()
                .answer(1)
                .articleId(articleId)
                .topicId(topic.getTopicId())
                .userId("egakHier")
                .build();
//        return VotingResult.fromUserVotings(topic, Arrays.asList(v1, v2, v3, v4, v5));
        return null;
    }

}

