package bbqbackend.controller;

import bbqbackend.model.repository.TopicStore;
import bbqbackend.model.topic.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 *
 */
@RestController
public class TopicsAndChallengeCtrl {

    @Autowired
    TopicStore topicStore;

    @RequestMapping(value = "/topics", method = RequestMethod.GET)
    public Set<Topic> getHotTopics(){
        Set<Topic> res;

        res = topicStore.todaysTopics();

        return res;
    }




}
