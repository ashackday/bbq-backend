'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var watch = require('gulp-watch');
var plumber = require('gulp-plumber');

gulp.task('sass', function () {
    gulp.src('assets/scss/*.scss')
        .pipe(plumber({
            errorHandler: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: ['bower_components/foundation/scss'],
            outputStyle: 'compressed'
        }))

        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('../assets/css'));
});

// Default task
gulp.task('default', ['sass']);

// Watch task
gulp.task('watch', ['sass'], function () {
    gulp.watch('assets/scss/*.scss', ['sass']);
});