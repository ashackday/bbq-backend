<#-- Layout macros -->

<#-- Macro for HTML5 skeleton with foundation -->
<#macro htmlContainer>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Haxelthon | Welcome</title>

    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="/assets/vendors/owl-carousel/owl.carousel.css">

    <!-- Default Theme -->
    <link rel="stylesheet" href="/assets/vendors/owl-carousel/owl.theme.css">

    <!-- Main Style -->
    <link rel="stylesheet" href="/assets/css/main.css" />
    <script src="/assets/js/vendor/jquery.js"></script>
    <script src="/assets/js/vendor/modernizr.js"></script>
    <script src="/assets/js/Chart.min.js"></script>

</head>
<body>
    <#nested >
</body>
</html>
</#macro>


<#-- Remove bad text entity -->
<#function removeBadText text>
    <#local text = text />

    <#return text?replace('Ã¼','ü')?replace('Ã¶','ö')?replace('Ã¤','ä')?replace('â ','&mdash;')?replace('â&#128;&#158;','"')?replace('â&#128;&#156;','"')>
</#function>
