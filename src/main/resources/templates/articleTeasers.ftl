<#-- Article teasers view -->
<#import 'layout/html.ftl' as layout>

<#-- ArticleUrl -->
<#assign articleUrl = "articlesView/article/" />

<#compress>
    <@layout.htmlContainer>
        <div id="owl" class="owl-carousel">
            <#list articles as article >
                <#-- Define modell var's -->
                <#assign articleHeadline = (article.headline) ! "Test headline" />
                <#assign articleId = (article.articleId.id) ! "Test id" />
                <#assign teaserText = (article.teaserText) ! "Test text" />
                <#assign teaserImg = (article.teaserImg) ! "/assets/img/artbg2.png" />
                <#assign assignedTopicName = (article.assignedTopic.name) ! "Test topic name" />
                <#assign assignedTopic = (article.assignedTopic) ! "Test assign topic" />
                <a href="btnClick://${articleUrl}${articleId}">
                    <div class="teaser-container">
                        <div class="content">
                            <h1>
                                ${layout.removeBadText(articleHeadline)}
                            </h1>
                            <h2>${layout.removeBadText(teaserText)}</h2>
                        </div>
                        <img src="${teaserImg}" alt="Article Img"/>
                    </div>
                </a>
            </#list>
        </div>

        <!-- Include js plugin for owl -->
        <script src="/assets/vendors/owl-carousel/owl.carousel.js"></script>
        <script>
            $(document).ready(function () {
                $("#owl").owlCarousel({
                    center: true,
                    items: 1.25,
                    loop: true,
                    margin: 10,
                    autoplay: true,
                    autoHeight : false,
                    margin: 30,
                });
            });
        </script>
    </@layout.htmlContainer>
</#compress>