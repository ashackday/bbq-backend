<#-- Challenge result view -->
<#import 'layout/html.ftl' as layout>

<#-- votingResult -->
<#-- Mock BE - Objekt -->
<#assign votesPerOptionMock = {'challengeName':'challengeName',
                            'articleVotes': [{ 'articleHeadline': 'articleHeadline',
                                                'optionsResults' : {
                                                                'Gut' : 20,
                                                                'Schlecht': 15
                                                                }
                                            },
                                            { 'articleHeadline': 'articleHeadline2',
                                            'optionsResults' : {
                                                            'Gut' : 29,
                                                            'Schlecht': 30
                                                            }
                                            }
                                            ]
                                } />


<#assign votesPerOptionMock = votingResult ! {} />

<#compress>
    <@layout.htmlContainer>
        <div class="challenge-result-container">
            <div class="challenge">
                <h1>${votesPerOptionMock.challengeName}</h1>
            </div>

            <#list votesPerOptionMock.articleVotes as votePerOption>
                <div class="results">
                    <div class="article-chart">
                        <canvas id="chart-area${votePerOption_index}" class="polar-chart" width="125" height="125"/>
                    </div>

                    <div class="article-description">
                        <h2>${votePerOption.articleHeadline}</h2>
                    </div>
                </div>
                <div id="chart${votePerOption_index}" class="legende"></div>
            </#list>
        </div>

        <script type="text/javascript">
            <#list votesPerOptionMock.articleVotes as votePerOption>
                var polarData${votePerOption_index} = [
                        <#list votePerOption.optionResults?keys as key>
                            {
                                value: ${votePerOption.optionResults[key]}, // Math.random()*100
                                color: "<#if key == "Gut">#46BFBD<#else>#F7464A</#if>",
                                highlight: "<#if key == "Gut">#5AD3D1<#else>#FF5A5E</#if>",
                                label: "${key}"
                            }
                            <#if key_has_next>,</#if>
                        </#list>
                    ];

                    $(function() {
                        var ctx${votePerOption_index} = document.getElementById("chart-area${votePerOption_index}").getContext("2d");
                        window.myPolarArea${votePerOption_index} = new Chart(ctx${votePerOption_index}).PolarArea(polarData${votePerOption_index}, {
                            responsive: false,
                            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li style=\"background-color:<%=segments[i].fillColor%>\"> <%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"

                        });
                        var legend${votePerOption_index} = window.myPolarArea${votePerOption_index}.generateLegend();
                        $('#chart${votePerOption_index}').append(legend${votePerOption_index});
                    });
            </#list>
        </script>
    </@layout.htmlContainer>
</#compress>