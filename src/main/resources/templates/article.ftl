<#-- Article teasers view -->
<#import 'layout/html.ftl' as layout>

<#-- Define modell var's -->
<#assign articleHeadline = (article.headline) ! "Test headline" />
<#assign articleId = (article.articleId.id) ! "Test id" />
<#assign articleImg = (article.imageUrl) ! "/assets/img/article-fallback.png" />
<#assign articleText = (article.content) ! "Test content text" />
<#assign teaserText = (article.teaserText) ! "Test teaser text" />
<#assign topicName = (topic.name) ! "Test topic name" />
<#assign topicChallengeQuestion = (topic.challenge.question) ! "Test challenge question" />
<#assign topiChallengeOptions = (topic.challenge.options) ! "Test challenge options" />

<#compress>
    <@layout.htmlContainer>
        <div class="article-container">
            <img src="${articleImg}" alt="Article image"/>
            <h1>
                ${layout.removeBadText(articleHeadline)}
            </h1>
            <p>
                ${layout.removeBadText(articleText?substring(0, 400))}
            </p>
            <p>
                ${layout.removeBadText(articleText?substring(401, 1000))}
            </p>
            <p>
                ${layout.removeBadText(articleText?substring(1001, 1200))} ...
            </p>
        </div>
    </@layout.htmlContainer>
</#compress>